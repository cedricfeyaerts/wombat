FROM debian:9
LABEL vendor="Cedric Feyaetrs"

RUN true 1 && apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
RUN echo 'LANG="en_US.UTF-8"'>/etc/default/locale
RUN dpkg-reconfigure --frontend=noninteractive locales
RUN update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8

RUN apt-get install -y curl
RUN apt-get install -y autoconf bison build-essential libcurl4-openssl-dev libffi-dev libgdbm-dev libgdbm3 libicu-dev libncurses5-dev libpq-dev libreadline-dev libreadline6-dev libsqlite3-dev libssl-dev libxml2-dev libxslt1-dev libyaml-dev sqlite3 zlib1g-dev libjemalloc-dev libjemalloc1 git
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg |  apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
run apt-get install -y nodejs yarn
RUN mkdir /tmp/ruby
RUN cd /tmp/ruby && curl --remote-name --progress https://cache.ruby-lang.org/pub/ruby/2.5/ruby-2.5.3.tar.bz2
RUN cd /tmp/ruby && echo '228a787ba68a7b20ac6e1d5af3d176d36e8ed600eb754d6325da341c3088ed76  ruby-2.5.3.tar.bz2' | sha256sum -c - && tar xjf ruby-2.5.3.tar.bz2
RUN cd /tmp/ruby && cd ruby-2.5.3 && ./configure --with-jemalloc --disable-install-rdoc && make && make install && ruby --version
RUN rm -rf tmp/ruby
RUN gem install bundler -v '~> 1.16.0' --no-ri --no-rdoc

# config git
run git config --global user.email "wombat@adhoc-gti.com"
RUN git config --global user.name "wombat"

# ENV APP_NAME www
ENV INSTALL_PATH /opt/app
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

ADD Gemfile* $INSTALL_PATH/
RUN bundle install --path ../shared/bundle

add . $INSTALL_PATH

RUN yarn
RUN bundle install --path ../shared/bundle
RUN bundle exec rake assets:precompile
RUN mkdir wiki
RUN ["chmod", "+x", "./docker_entrypoint.sh"]

EXPOSE 3000
ENTRYPOINT ["./docker_entrypoint.sh"]
# CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
