namespace :repository do
  desc 'Reset all'
  task reset: [:environment] do
    Entry.destroy_all
    Tag.destroy_all
    Wiki.wiki.pages.each do |page|
      entry = Entry.create(path: page.url_path, content: page.raw_data)
      Tag.parse(entry)
    end
  end

  desc 'Push all'
  task sync: [:environment] do
    Wiki.sync
  end
end