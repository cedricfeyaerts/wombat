class CreateEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :entries do |t|
      t.string :path
      t.text :content

      t.timestamps
    end
    add_index :entries, :path
  end
end
