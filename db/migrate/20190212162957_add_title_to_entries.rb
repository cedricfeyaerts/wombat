class AddTitleToEntries < ActiveRecord::Migration[5.2]
  def change
    add_column :entries, :title, :string
    add_index :entries, :title
  end
end
