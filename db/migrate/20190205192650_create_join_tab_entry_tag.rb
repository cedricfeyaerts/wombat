class CreateJoinTabEntryTag < ActiveRecord::Migration[5.2]
  create_join_table :entries, :tags do |t|
    t.index [:entry_id, :tag_id]
  end
end
