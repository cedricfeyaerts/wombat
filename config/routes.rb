Rails.application.routes.draw do
  resources :tags do
    get :search, on: :collection
    resources :entries, only: :index do
      get :search, on: :collection
    end
  end
  resources :entries do
    get :search, on: :collection
  end
  resource :repository, only: [] do
    post :sync
  end
  resources :paths, only: :index
  resources :sessions, only: :new
  post '/auth/:provider/callback', to: 'sessions#create'
  get '/auth/:provider/callback', to: 'sessions#create'

  get '/(*path)(.*format)', to: 'welcome#index'
end
