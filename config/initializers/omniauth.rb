Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :gitlab, ENV['GITLAB_KEY'], ENV['GITLAB_SECRET'],
    {
      scope: 'read_user email',
      client_options: {
        site: ENV['GITLAB_URL']
      }
    }
end