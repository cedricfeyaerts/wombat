#!/bin/bash

bundle exec rake db:migrate
git clone $GIT_REPO wiki
bundle exec rake repository:reset
bundle exec puma -C config/puma.rb
