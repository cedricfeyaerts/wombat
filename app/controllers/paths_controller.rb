class PathsController < ApplicationController

  # GET /paths
  # GET /paths.json
  def index
    paths = Entry.with_directory.search_path(params[:s]).pluck(:path)

    @dirs = paths.map do |path|
      File.dirname(path)
    end.uniq.sort

    render json: @dirs
  end
end
