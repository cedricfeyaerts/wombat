class RepositoriesController < ApplicationController

  # GET /paths
  # GET /paths.json
  def sync
    if Wiki.sync
      render json: { status: 'ok' }
    else
      render json: { status: 'nok' }, status: :unprocessable_entity
    end
  end
end
