class EntriesController < ApplicationController
  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  # GET /entries
  # GET /entries.json
  def index
    @entries = scope.order(updated_at: :desc)
  end

  def search
    render json: scope.search(params[:s])
  end

  # GET /entries/1
  # GET /entries/1.json
  def show; end

  # GET /entries/new
  def new
    @entry = Entry.new
  end

  # GET /entries/1/edit
  def edit; end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(entry_params)

    respond_to do |format|
      if @entry.save
        Tag.parse(@entry)
        Tag.clean
        Wiki.save(@entry, current_user)
        format.html { redirect_to @entry, notice: 'Entry was successfully created.' }
        format.json { render :show, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    respond_to do |format|
      old_path = @entry.path
      if @entry.update(entry_params)
        Wiki.rename(@entry, old_path, current_user)
        Wiki.save(@entry, current_user)
        Tag.parse(@entry)
        Tag.clean
        format.html { redirect_to @entry, notice: 'Entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    Tag.clean
    Wiki.delete(@entry, current_user)
    respond_to do |format|
      format.html { redirect_to entries_url, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:path, :content)
    end

    def scope
      return @scope unless @scope.nil?
      return @scope = Entry.all unless params[:tag_id]

      @scope = Tag.find(params[:tag_id]).entries
    end
end
