class ApplicationController < ActionController::Base
  before_action :authenticate

  def current_user=(user)
    session[:user_id] = user.id
  end

  def current_user
    return unless session[:user_id]

    User.find(session[:user_id])
  end

  def authenticate
    return if current_user

    redirect_to new_session_path
  end
end
