class WelcomeController < ApplicationController

  def index;
    return unless params[:path]

    @entry = Entry.find_by(path: [params[:path],params[:format]].compact.join('.'))
  end
end