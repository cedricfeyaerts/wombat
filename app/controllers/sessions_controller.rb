class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate

  def new; end

  def create
    @user = User.find_or_create_by(uid: auth_hash.uid, provider: auth_hash.provider)
    @user.email = auth_hash.info.email
    @user.name = auth_hash.info.name
    @user.save
    self.current_user = @user
    redirect_to '/'
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end