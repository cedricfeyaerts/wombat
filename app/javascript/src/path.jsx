import React from "react";
import "./path.scss";
import Storage from "./storage";

class Path extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      list: [],
      typeahead: null
    };
  }

  nextPath() {
    if (!this.state.typeahead) {
      return;
    }

    let path = this.props.entry.path;
    let typeahead = this.state.typeahead;
    let indexNextSlash = typeahead.indexOf("/", path.length);

    if (indexNextSlash === -1) {
      return typeahead;
    }

    return typeahead.slice(0, indexNextSlash);
  }

  cycleTypeahead(step) {
    let currentIndex = this.state.list.indexOf(this.state.typeahead);
    return this.state.list[currentIndex + step];
  }

  replaceTypeahead(path, newTypeahead) {
    console.log(path, newTypeahead, this.state.typeahead);

    if (this.state.typeahead == null || this.state.typeahead.indexOf(path) == -1) {
      return newTypeahead;
    }
    return this.state.typeahead;
  }

  onKeyDown(e) {
    switch (e.key) {
      case "ArrowUp": {
        this.setState({ typeahead: this.cycleTypeahead(-1) });
        break;
      }
      case "ArrowDown": {
        this.setState({ typeahead: this.cycleTypeahead(1) });
        break;
      }
      case "ArrowRight": {
        let nextPath = this.nextPath();

        if (
          e.target.selectionStart >= this.props.entry.path.length &&
          nextPath &&
          nextPath !== this.props.entry.path
        ) {
          let newEntry = { ...this.props.entry, path: nextPath };
          this.props.onChange(newEntry);
        }
        break;
      }
    }
  }

  onChange(e) {
    let value = e.currentTarget.value;
    this.props.onChange({ ...this.props.entry, path: value });
    if (value.length > 2) {
      Storage.loadSearchPath(value).then(list =>
        this.setState({ list: list, typeahead: this.replaceTypeahead(value, list[0]) })
      );
    }
  }

  render() {
    return (
      <section className="path">
        <div className="path__typeahead">{this.state.typeahead}</div>
        <input
          className="path__input"
          type="text"
          value={this.props.entry.path}
          onChange={this.onChange.bind(this)}
          onKeyDown={this.onKeyDown.bind(this)}
        />
      </section>
    );
  }
}

export default Path;
