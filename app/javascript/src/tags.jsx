import React from "react";
import Tag from "./tag";
import "./tags.scss";

const Tags = props => (
  <section className="tags">
    <header className="tags__header">Tags</header>
    {props.tags.map((tag, i) => (
      <Tag key={i} tag={tag} onClick={props.onSelect} />
    ))}
  </section>
);
export default Tags;
