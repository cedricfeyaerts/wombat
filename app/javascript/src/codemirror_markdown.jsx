import React from "react";
import CodeMirror from "codemirror";
import Storage from "./storage";

import "codemirror/lib/codemirror.css";
import "codemirror/mode/gfm/gfm";
import "codemirror/addon/edit/continuelist.js";
import "codemirror/addon/mode/overlay.js";
import "codemirror/addon/hint/show-hint.js";
import "codemirror/addon/hint/show-hint.css";
import "./codemirror_markdown.scss";

class CodeMirrorMarkDown extends React.Component {
  constructor(props) {
    super(props);
    // this.textareaNode = React.createRef();
    this.defaultOptions = {
      mode: { name: "gfm", highlightFormatting: true },
      backdrop: "gfm",
      lineNumbers: false,
      matchBrackets: true,
      lineWrapping: true,
      theme: "base16-light",
      extraKeys: {
        Enter: "newlineAndIndentContinueMarkdownList",
        "Ctrl-Space": "autocomplete"
      },
      hintOptions: { hint: this.onHint }
    };
    this.onValueChange = this.onValueChange.bind(this);
    this.selectionChanger = this.selectionChanger.bind(this);
  }

  onHint(cm, option) {
    let cursor = cm.getCursor();
    let line = cm.getLine(cursor.line);
    var start = cursor.ch;
    let end = cursor.ch;
    while (start && /[\/#\w]/.test(line.charAt(start - 1))) --start;
    while (end < line.length && /[\/\w]/.test(line.charAt(end))) ++end;
    let word = line.slice(start + 1, end).toLowerCase();
    let tag = line.slice(start, 1).toLowerCase();
    let output = {
      from: CodeMirror.Pos(cursor.line, start + 1),
      to: CodeMirror.Pos(cursor.line, end)
    };
    if (tag == "#") {
      return Storage.loadSearchTags(word).then(data => {
        return { ...output, list: data };
      });
    }
  }

  componentDidMount() {
    this.textareaNode = document.querySelector("#code");
    this.codeMirror = CodeMirror.fromTextArea(
      this.textareaNode,
      this.defaultOptions || this.props.options
    );
    this.codeMirror.on("change", this.onValueChange);
    // this.codeMirror.on('cursorActivity', this.cursorActivity);
    // this.codeMirror.on('focus', this.focusChanged.bind(this, true));
    // this.codeMirror.on('blur', this.focusChanged.bind(this, false));
    // this.codeMirror.on('scroll', this.scrollChanged);
    this.codeMirror.setValue(this.props.value || "");

    this.codeMirror.addKeyMap({
      "Ctrl-B": cm => cm.replaceSelection(this.selectionChanger(cm.getSelection(), "**")),
      "Ctrl-I": cm => cm.replaceSelection(this.selectionChanger(cm.getSelection(), "_")),
      "Ctrl-K": cm => cm.replaceSelection(this.selectionChanger(cm.getSelection(), "`"))
    });
  }

  componentWillUnmount() {
    if (this.codeMirror) {
      this.codeMirror.toTextArea();
    }
  }

  componentWillReceiveProps(nextProps) {
    let prevScrollPosition = this.codeMirror.getScrollInfo();
    let preCursor = this.codeMirror.getCursor();
    this.codeMirror.setValue(nextProps.value);
    this.codeMirror.scrollTo(prevScrollPosition.left, prevScrollPosition.top);
    this.codeMirror.setCursor(preCursor);
  }

  selectionChanger(selection, operator, endoperator) {
    if (selection === "") {
      return operator;
    }
    if (!endoperator) {
      endoperator = operator;
    }
    var isApplied = selection.slice(0, 2) === operator && selection.slice(-2) === endoperator;
    var finaltext = isApplied ? selection.slice(2, -2) : operator + selection + endoperator;
    return finaltext;
  }

  onValueChange(doc, change) {
    if (this.props.onChange && change.origin !== "setValue") {
      this.props.onChange(doc.getValue(), change);
    }
  }

  render() {
    return (
      <div className="codemirror-markdown">
        <textarea
          // ref={this.textareaNode}
          id="code"
          defaultValue={this.props.value}
          autoComplete="off"
        />
      </div>
    );
  }
}

export default CodeMirrorMarkDown;
