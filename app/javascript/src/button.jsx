import React from "react";
import "./button.scss";
import "material-design-icons/iconfont/material-icons.css";

const Button = props => {
  const className = `button material-icons ${props.action.disabled ? "button--disabled" : ""}`;
  return (
    <a
      href="#"
      className={className}
      onClick={e => {
        e.preventDefault();
        props.action.onClick(e);
      }}
    >
      {props.action.label}
    </a>
  );
};

export default Button;
