import React from "react";
import "./editor.scss";
import CodeMirrorMarkdown from "./codemirror_markdown.jsx";
import Path from "./path.jsx";

class Editor extends React.Component {
  onChangePath(e) {
    let newEntry = { ...this.props.entry };
    newEntry.path = e.currentTarget.value;
    this.props.onChange(newEntry);
  }

  onChangeContent(value) {
    let newEntry = { ...this.props.entry };
    newEntry.content = value;
    this.props.onChange(newEntry);
  }

  render() {
    return (
      <section className="editor">
        <Lock locked={this.props.locked} />
        <div className="editor__container">
          <Path entry={this.props.entry} onChange={this.props.onChange} />
          <section className="editor__content">
            <CodeMirrorMarkdown
              value={this.props.entry.content}
              onChange={this.onChangeContent.bind(this)}
            />
          </section>
        </div>
      </section>
    );
  }
}

const Lock = function(props) {
  if (props.locked) {
    return <div className="editor__lock" />;
  } else {
    return null;
  }
};

export default Editor;
