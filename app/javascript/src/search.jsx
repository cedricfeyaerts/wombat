import React from "react";
import "./search.scss";
import "material-design-icons/iconfont/material-icons.css";

class Search extends React.Component {
  onChange(e) {
    this.props.onSearch(e.currentTarget.value);
  }

  render() {
    return (
      <section className="search">
        <span className="search__icon material-icons">search</span>
        <input
          className="search__input"
          type="text"
          value={this.props.value}
          onChange={this.onChange.bind(this)}
          placeholder="Search"
        />
      </section>
    );
  }
}

export default Search;
