import React from "react";
import "./entry.scss";

const Entry = props => (
  <article
    i={props.entry.id}
    className={props.entry.selected ? "entry entry--selected" : "entry"}
    onClick={_ => props.onClick(props.entry)}
  >
    <header className="entry__title">{props.entry.title}</header>
    <div className="entry__content">{props.entry.content}</div>
  </article>
);

export default Entry;
