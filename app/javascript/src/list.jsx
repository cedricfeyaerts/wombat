import React from "react";
import Entry from "./entry";

const List = props =>
  props.entries.map((entry, i) => <Entry key={i} id={i} entry={entry} onClick={props.onSelect} />);

export default List;
