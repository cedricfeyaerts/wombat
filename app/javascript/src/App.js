import React, { Component } from "react";
import "./App.css";
import Sidebar from "./sidebar";
import Editor from "./editor";
import Menu from "./menu";
import Storage from "./storage";

class App extends Component {
  constructor(props) {
    super(props);
    let current = { path: "", content: "# ", id: null };
    if (
      document.querySelector("#root").dataset.entry &&
      document.querySelector("#root").dataset.entry !== "null"
    ) {
      current = JSON.parse(document.querySelector("#root").dataset.entry);
    }
    let settings = {
      readOnly: false
    };
    if (localStorage.settings) {
      settings = JSON.parse(localStorage.settings);
    }

    this.state = {
      tags: [],
      selectedTagId: null,
      entries: [],
      current: current,
      searchQuery: "",
      saved: false,
      locked: false,
      settings: settings
    };

    Storage.loadAllTags().then(this.setTags.bind(this));
    Storage.loadAllEntries().then(this.setEntries.bind(this));
    this.onSelectEntry = this.onSelectEntry.bind(this);
    this.csrfToken = document.querySelector('meta[name = "csrf-token"]').getAttribute("content");
    this.csrfParam = document.querySelector('meta[name = "csrf-param"]').getAttribute("content");
  }

  setTags(tags) {
    this.setState({ ...this.state, tags });
  }
  setEntries(entries) {
    this.setState({ ...this.state, entries });
  }
  sync() {
    Storage.sync(this.csrfToken).then(result => {
      if (result.status === "ok") this.setState({ sync: true });
    });
  }

  actions() {
    let actions = [];
    actions.push({ label: "create", onClick: this.onNewEntry.bind(this) });
    actions.push({
      label: "delete",
      onClick: this.onDeleteEntry.bind(this),
      disabled: this.state.current.id === null
    });
    if (this.state.settings.readOnly) {
      actions.push({
        label: "lock",
        onClick: this.onSetSettings.bind(this, { readOnly: false })
      });
    } else {
      actions.push({
        label: this.state.sync ? "done_all" : "done",
        onClick: this.onSaveEntry.bind(this),
        disabled: this.state.saved
      });
    }
    return actions;
  }

  onKeyDown(e) {
    if (!e.ctrlKey || e.key == "Control") {
      return;
    }
    switch (e.key) {
      case "s": {
        this.onSaveEntry();
        break;
      }
      case "n": {
        this.onNewEntry();
        break;
      }
      case "l": {
        this.onSetSettings({ readOnly: true });
        break;
      }
    }
  }

  onSetSettings(newSettings) {
    let settings = Object.assign({}, this.state.settings, newSettings);
    this.setState({ settings: settings });
  }

  onNewEntry() {
    this.setState({ current: { path: "", content: "# ", id: null, saved: false, sync: false } });
  }

  onDeleteEntry() {
    if (!confirm("I have a bad feeling about this.")) {
      return;
    }
    this.setState({ locked: true });
    Storage.delete(this.state.current, this.csrfToken).finally(() => {
      // Storage.resetLocally();
      this.setState({
        current: { path: "", content: "", id: null },
        saved: false,
        locked: false,
        sync: false
      });
      Storage.loadAllTags().then(this.setTags.bind(this));
      Storage.loadAllEntries().then(this.setEntries.bind(this));
      this.sync();
    });
  }

  onSaveEntry() {
    this.setState({ locked: true });
    Storage.save(this.state.current, this.csrfToken).then(entry => {
      this.setState({ current: entry, saved: true, locked: false });
      // Storage.resetLocally();
      Storage.loadAllTags().then(this.setTags.bind(this));
      Storage.loadAllEntries().then(this.setEntries.bind(this));
      this.sync();
    });
  }

  onSelectEntry(selectedEntry) {
    let id = selectedEntry.id;
    let newEntries = Storage.selectById(id, this.state.entries);
    let newEntry = { ...selectedEntry };

    history.pushState({}, "", `/${selectedEntry.path}`);
    this.setState({ current: newEntry, entries: newEntries, saved: true });
    Storage.resetLocally();
  }

  onSelectTag(tag) {
    let id = tag.id;
    let tags = Storage.selectById(id, this.state.tags);

    this.setState({
      selectedTagId: id,
      tags: tags
    });
    if (id) {
      Storage.loadTaggedEntries(id).then(this.setEntries.bind(this));
    } else {
      Storage.loadAllEntries().then(this.setEntries.bind(this));
    }
  }

  onEditorChange(entry) {
    if (this.state.settings.readOnly) {
      this.setState({ current: this.state.current });
      return;
    }
    history.replaceState({}, "", `/${entry.path}`);
    this.setState({ current: this.ensureFormatEntry(entry), saved: false, sync: false });
    Storage.saveLocally(this.state.current);
  }

  ensureFormatEntry(entry) {
    if (new RegExp(/^# .*/m).test(entry.content)) {
      return entry;
    }

    return { ...entry, content: "# " + entry.content };
  }

  onSearch(query) {
    this.setState({ searchQuery: query });
    Storage.loadSearchEntries(query).then(this.setEntries.bind(this));
  }

  render() {
    return (
      <main className="App" onKeyDown={this.onKeyDown.bind(this)} tabIndex="0">
        <Sidebar
          tags={this.state.tags}
          onSelectTag={this.onSelectTag.bind(this)}
          entries={this.state.entries}
          onSelectEntry={this.onSelectEntry}
          searchQuery={this.state.searchQuery}
          onSearch={this.onSearch.bind(this)}
        />
        <Editor
          locked={this.state.locked}
          entry={this.state.current}
          onChange={this.onEditorChange.bind(this)}
          readOnly={this.state.settings.readOnly}
        />
        <Menu actions={this.actions()} />
      </main>
    );
  }
}

export default App;
