class Storage {
  static loadAllTags = () =>
    Storage.loadData("/tags.json")
      .then(Storage.reduceTags)
      .then(Storage.addAllToTags);
  static loadAllEntries = () => Storage.loadData("/entries.json");
  static loadSearchEntries = query => Storage.loadData(`/entries/search?s=${query}`);
  static loadTaggedEntries = id => Storage.loadData(`/tags/${id}/entries/search`);
  static loadSearchPath = query => Storage.loadData(`/paths?s=${query}`);
  static loadSearchTags = query => Storage.loadData(`/tags/search?s=${query}`);

  static loadData(url) {
    return fetch(url, {
      method: "GET"
    }).then(res => res.json());
  }

  static selectById(id, array) {
    return array.map((tag, i) => {
      let selected = tag.id == id;
      if (tag.children) tag.children = Storage.selectById(id, tag.children);
      return { ...tag, selected };
    });
  }

  static reduceTags(tags) {
    return tags.reduce(Storage.recurse);
  }

  static addAllToTags(tags) {
    return [
      {
        label: "All",
        id: null
      },
      ...tags
    ];
  }

  static recurse(acc, next) {
    if (!Array.isArray(acc)) {
      acc = [acc];
    }
    let last = [...acc].pop();
    next = { ...next };
    if (last == next) return acc;
    if (!next.label.startsWith(last.label + "/")) return [...acc, next];
    next.label = next.label.replace(last.label + "/", "");
    if (!last.children) {
      last.children = [next];
    } else {
      last.children = Storage.recurse(last.children, next);
    }
    return [...acc.slice(0, -1), last];
  }

  static saveLocally(entry) {
    localStorage.current = JSON.stringify(entry);
  }

  static resetLocally() {
    localStorage.current = null;
  }

  static save(entry, token) {
    let options = { ...Storage.fetchOptions(token), body: JSON.stringify({ entry: entry }) };
    if (entry.id) {
      return fetch(`/entries/${entry.id}`, { ...options, method: "PUT" }).then(res => res.json());
    } else {
      return fetch("/entries", { ...options, method: "POST" }).then(res => res.json());
    }
  }

  static sync(token) {
    let options = { ...Storage.fetchOptions(token), body: null };
    return fetch("/repository/sync", { ...options, method: "POST" }).then(res => res.json());
  }

  static delete(entry, token) {
    if (!entry.id) {
      return;
    }
    let options = Storage.fetchOptions(token, "DELETE");
    return fetch(`/entries/${entry.id}`, options);
  }

  static cloudSync(token) {
    let options = Storage.fetchOptions(token, "POST");
    return fetch(`/repository/sync`, options).then(res => console.log(res.ok));
  }

  static fetchOptions(token, method = "GET") {
    return {
      method: method,
      credentials: "include",
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "X-CSRF-Token": token,
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };
  }
}

export default Storage;
