import React from "react";
import Toggle from "./toggle";
import "./tag.scss";

const Subtags = props => {
  if (props.tags && props.open) {
    return (
      <section className="subtags">
        {props.tags.map((tag, i) => (
          <Tag key={i} tag={tag} onClick={props.onClick} />
        ))}
      </section>
    );
  } else {
    return null;
  }
};

const ToggleButton = props => {
  if (props.display) {
    return <Toggle open={props.open} onClick={props.onClick} />;
  }

  return null;
};

class Tag extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  onToggle() {
    console.log(this.state.open);
    this.setState({ open: !this.state.open });
  }

  render() {
    return (
      <div>
        <article className={this.props.tag.selected ? "tag tag--selected" : "tag"}>
          <ToggleButton
            open={this.state.open}
            onClick={this.onToggle.bind(this)}
            display={this.props.tag.children}
          />
          <span onClick={_ => this.props.onClick(this.props.tag)}>{this.props.tag.label}</span>
        </article>
        <Subtags
          tags={this.props.tag.children}
          onClick={this.props.onClick}
          open={this.state.open}
        />
      </div>
    );
  }
}

export default Tag;
