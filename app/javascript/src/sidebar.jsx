import React from "react";
import Search from "./search";
import List from "./list";
import Tags from "./tags";
import "./sidebar.scss";

class Sidebar extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Tags tags={this.props.tags} onSelect={this.props.onSelectTag} />
        <section className="sidebar">
          <Search value={this.props.searchQuery} onSearch={this.props.onSearch} />
          <List entries={this.props.entries} onSelect={this.props.onSelectEntry} />
        </section>
      </React.Fragment>
    );
  }
}

export default Sidebar;
