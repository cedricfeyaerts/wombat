import React from "react";
import "material-design-icons/iconfont/material-icons.css";
import "./toggle.scss";

const Toggle = props => {
  const className = `toggle material-icons ${props.open ? "toggle--open" : ""}`;
  return (
    <a
      href="#"
      className={className}
      onClick={e => {
        e.preventDefault();
        props.onClick(e);
      }}
    >
      {props.open ? "keyboard_arrow_down" : "keyboard_arrow_right"}
    </a>
  );
};

export default Toggle;
