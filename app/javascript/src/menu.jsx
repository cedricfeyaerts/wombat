import React from "react";
import Button from "./button";
import "./menu.scss";

const Menu = props => {
  return (
    <section className="menu">
      {props.actions.map((action, i) => {
        return <Button key={i} action={action} />;
      })}
    </section>
  );
};
export default Menu;
