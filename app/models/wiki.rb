class Wiki
  def self.wiki
    @wiki unless @wiki.nil?

    @wiki = Gollum::Wiki.new("wiki/.git", :base_path => "/wiki")
  end

  def self.commit(message, user)
    {
      message: message,
      name: user.name,
      email: user.email
    }
  end

  def self.save(entry, user)
    if (page = find_page(entry.path))
      wiki.update_page(page, page.name, :markdown, entry.content, commit("Update #{entry.path}", user))
    else
      url_path = wiki.preview_page(entry.path, entry.content, :markdown).url_path
      wiki.write_page(entry.path, :markdown, entry.content, commit("Create #{entry.path}", user))
      entry.update(path: url_path)
    end
  end

  def self.delete(entry, user)
    return unless (page = find_page(entry.path))

    wiki.delete_page(page, commit("Delete #{entry.path}", user))
  end

  def self.rename(entry, old_name, user)
    return unless (page = find_page(old_name))
    return if entry.path == old_name || entry.path.blank?

    wiki.rename_page(page, entry.path, commit("Rename #{old_name} into #{entry.path}", user))
  end

  def self.find_page(path)
    dir = File.dirname(path)
    name = File.basename(path)

    wiki.paged(name, dir)
  end

  def self.read_all
    wiki.pages.each do |page|
      entry = Entry.create(path: page.url_path, content: page.raw_data)
      Tag.parse(entry)
    end
  end

  def self.sync
    `cd wiki && git stash && git fetch && git rebase origin/master && git push && git stash pop`
  end
end
