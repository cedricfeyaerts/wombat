class Entry < ApplicationRecord
  has_and_belongs_to_many :tags

  before_save :parse_title, :ensure_path

  def parse_title
    return if content.nil?

    title_string = content.scan(/^# .*/).first
    return title = 'untitled' if title_string.blank?

    self.title = title_string.slice(1..-1).strip
  end

  def ensure_path
    self.path = title if path.blank?
  end

  def self.search(query)
    return all if query.blank?

    where("path LIKE :query OR title LIKE :query OR content LIKE :query", query: "%#{query}%")
  end

  def self.create_by_file(file_path, relative_path: '.')
    file_name = Pathname.new(file_path).relative_path_from(Pathname.new(relative_path))
    entry = Entry.find_or_create_by(path: file_name.to_s)
    entry.update(content: File.read(file_path))
    Tag.parse(entry)
  end

  def self.with_directory
    where('path LIKE :query', query: '%/%')
  end

  def self.search_path(query)
    return all if query.blank?

    where("path LIKE :query", query: "#{query}%")
  end
end
