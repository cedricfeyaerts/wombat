class Repository < ApplicationRecord

  def write_and_push(entry)
    write(entry)
    git_commit
    git_rebase
    git_push
    read_all
  end

  def remove_and_push(entry)
    remove(entry)
    git_commit
    git_rebase
    git_push
  end

  def sync
    git_rebase
    write_all
    git_commit
    git_push
    read_all
  end

  private

  def read_all
    file_list.each do |file_entry|
      Entry.create_by_file(file_entry, relative_path: path)
    end

    Entry.where.not(path: relative_file_list).destroy_all
  end

  def write_all
    Entry.all.each do |entry|
      write(entry)
    end
  end

  def git_rebase
    p `cd #{path} && git fetch && git rebase`
  end

  def git_push
    p `cd #{path} && git push`
  end

  def write(entry)
    full_path = File.join(path, entry.path)
    dir = File.dirname(full_path)
    FileUtils.mkdir_p(dir)
    File.write(full_path, entry.content)
  end

  def remove(entry)
    full_path = File.join(path, entry.path)
    File.delete(full_path)
  end

  def git_commit
    p `cd #{path} && git add . && git commit -m "Update at #{Time.zone.now}"`
  end

  def file_list
    Dir[File.join(path, '**/*')].select { |f| File.file?(f) }
  end

  def relative_file_list
    file_list.map do |file_path|
      Pathname.new(file_path).relative_path_from(Pathname.new(path)).to_s
    end
  end
end
