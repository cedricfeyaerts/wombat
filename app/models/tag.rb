class Tag < ApplicationRecord
  has_and_belongs_to_many :entries

  def self.parse(entry)
    entry.tags = []
    entry.content.scan(/#[\w\/]+/).each do |tag_string|
      label = tag_string.slice(1..-1)

      create_tags(label, entry)
    end
    dir = File.dirname(entry.path)
    return if dir.blank?

    create_tags(dir, entry)
  end

  def self.create_tags(label_string, entry)
    collector = []
    label_string.split('/').reduce(nil) do |x, acc|
      collector << [x, acc].compact.join('/')
      collector.last
    end


    collector.each do |label|
      create_tag(label, entry)
    end
  end

  def self.create_tag(label, entry)
    tag = Tag.find_or_create_by(label: label)
    entries = tag.entries
    entries << entry
    tag.entries = entries.uniq
  end

  def self.clean
    Tag.includes(:entries).where(entries_tags: { tag_id: nil }).destroy_all
  end

  def self.search(query)
    return all if query.blank?

    where("label LIKE :query", query: "%#{query}%")
  end
end
